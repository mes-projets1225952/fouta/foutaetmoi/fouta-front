import { EventEmitter, Injectable } from '@angular/core';
import { Site } from '../Model/site.model';
import {HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
import { Hotel } from '../Model/hotel.model';
import { Guide } from '../Model/guide.model';

@Injectable({
  providedIn: 'root'
})
export class SiteService {
  private sites!: Site [];
  private hotel!:Hotel [];
  private guide!:Guide [];
  url='http://127.0.0.1:3030/api/';
  

  constructor( private http: HttpClient) { }
//Liste des sites
  getSite():Observable<Site []>{
    return this.http.get<Site[]>(this.url+ 'site/');
  }
//Liste des hotels
  getSHotel():Observable<Hotel []>{
    return this.http.get<Hotel[]>(this.url+ 'hotel/');}
//Ajout d'un site    
  addSite(site:Site):Observable<any>{
      return this.http.post(this.url + 'site/ajout/', site);

    }
//Ajout d'un hotel    
  addHotel(hotel:Hotel): Observable<any>{
    return this.http.post(this.url+ 'hotel/ajout/', hotel);
    }
//Liste guide   
  getGuide():Observable<Guide []>{
    return this.http.get<Guide[]>(this.url+ 'guide/');
  } 
//Ajout d'un guide       
  addGuide(data:any){
    return this.http.post(this.url+ 'guide/ajout/', data);
      }
//Liste site par id      
  geSiteById(id:number):Observable<Site> {
    return this.http.get<Site>(this.url+ 'site/'+ id);
      }
//Liste hotel par id      
  geHotelById(id:number):Observable<Hotel> {
    return this.http.get<Hotel>(this.url+ 'hotel/'+ id);
      }
//Liste guide par id      
  geGuideById(id:number):Observable<Guide> {
    return this.http.get<Guide>(this.url+ 'guide/'+ id);
      }
//Suppression d'un site  
  DeleteSite(id:number):Observable<Site> {
    console.log(id)
    return this.http.delete<Site>(this.url+ 'site/'+ id);
  }
  //Suppression d'un  Guide
  DeleteGuide(id:number):Observable<Guide> {
    console.log(id)
    return this.http.delete<Guide>(this.url+ 'guide/'+ id);
  }
  //Suppression d'un hotel
  DeleteHotel(id:number):Observable<Hotel> {
    console.log(id)
    return this.http.delete<Hotel>(this.url+ 'hotel/'+ id);
  }
//Modification d'un site  
  updateSite(id:number, site:Site):Observable<Site> {
    return this.http.put<Site>(this.url+ 'site/'+ id+'/', site)
  }
  //Modification d'un hotel  
  updateHotel(id:number, hotel:Hotel):Observable<Hotel> {
    return this.http.put<Hotel>(this.url+ 'hotel/'+ id+'/', hotel)
  }
  //Modification d'un guide  
  updateGuide(id:number, guide:Guide):Observable<Guide> {
    return this.http.put<Guide>(this.url+ 'guide/'+ id+'/', guide)
  }
// Detail d'un hotel (communication entre composant)
  onShowDeteailsClicked=new EventEmitter<Hotel>() ;
  showHoteleDetails(hotel:Hotel){
    this.onShowDeteailsClicked.emit(hotel);
  }  
  getBase64(file:any): Observable<string> {
    return new Observable<string>(sub => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        sub.next(reader.result!.toString());
        sub.complete();
      };
      reader.onerror = error => {
        sub.error(error);
      };
    })
  }
}

