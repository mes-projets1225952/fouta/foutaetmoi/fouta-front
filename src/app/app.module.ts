import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SiteModule } from './Site/site.module';
import { RouterModule, Routes } from '@angular/router';

import { GuideModule } from './Guide/guide.module';
import { HotelModule } from './Hotel/hotel.module';
import { ListeHotelComponent } from './Hotel/liste-hotel/liste-hotel.component';
import { HotelsComponent } from './Hotel/hotels/hotels.component';
import { DetailHotelComponent } from './Hotel/detail-hotel/detail-hotel.component';
import { UpdateHotelComponent } from './Hotel/update-hotel/update-hotel.component';
import { DeleteGuidesComponent } from './Guide/delete-guides/delete-guides.component';
import { PrimeIcons } from 'primeng/api';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SiteModule,
    GuideModule,
    HotelModule,
   
  
  ],

  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
