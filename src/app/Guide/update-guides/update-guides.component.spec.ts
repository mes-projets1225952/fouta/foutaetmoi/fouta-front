import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateGuidesComponent } from './update-guides.component';

describe('UpdateGuidesComponent', () => {
  let component: UpdateGuidesComponent;
  let fixture: ComponentFixture<UpdateGuidesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UpdateGuidesComponent]
    });
    fixture = TestBed.createComponent(UpdateGuidesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
