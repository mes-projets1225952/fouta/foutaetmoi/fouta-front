import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { Guide } from 'src/app/Model/guide.model';
import { Site } from 'src/app/Model/site.model';
import { SiteService } from 'src/app/Service/site.service';

@Component({
  selector: 'app-update-guides',
  templateUrl: './update-guides.component.html',
  styleUrls: ['./update-guides.component.css']
})
export class UpdateGuidesComponent implements OnInit{
  public guide: Guide=new Guide();
  public guideForm!: FormGroup;
  selectedFile!: File;
  errorMessage!: string;
  site!: Site [];
  id_guide: any;
  constructor( private service: SiteService,
              private router: Router,
              private route: ActivatedRoute
    ){}
  ngOnInit(): void {
    this.guideForm=new FormGroup({
      nom:new FormControl(),
      age:new FormControl(),
      tel:new FormControl(),
      mail:new FormControl(),
      site_T:new FormControl()
    });
    this.service.getSite().subscribe(
      (site)=>{this.site= site
        console.log(this.site)
      },
      (error)=>{
        alert("Problème d'accès à l'API")
        console.log(error)
      }
        )
        this.id_guide=this.route.snapshot.params['id']
        this.service.geGuideById(this.id_guide).subscribe(data=>{
          this.guideForm.patchValue(data)
          this.guide=data
          console.log(this.guide)
        })
  }
  
public updateGuide() :void{
  var formData:any=new FormData
  formData.append('nom', this.guideForm.get('nom')!.value);
  formData.append('age', this.guideForm.get('age')!.value);
  formData.append('tel', this.guideForm.get('tel')!.value);
  formData.append('mail', this.guideForm.get('mail')!.value);
  formData.append('site_T', this.guideForm.get('site_T')!.value);
  this.service.updateGuide(this.id_guide,formData).subscribe((data)=>{
    console.log(data)
    this.router.navigate(['guide'])
  },
  (error)=>{
    console.log(error)
  })
}

}



