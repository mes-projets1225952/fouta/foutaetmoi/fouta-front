import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { GuidesComponent } from './guides/guides.component';
import { UpdateGuidesComponent } from './update-guides/update-guides.component';
import { DeleteGuidesComponent } from './delete-guides/delete-guides.component';
import { DetailGuidesComponent } from './detail-guides/detail-guides.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TreeTableModule } from 'primeng/treetable';
import { AddGuideComponent } from './add-guide/add-guide.component';
import { TabViewModule } from 'primeng/tabview';
import { ButtonModule } from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';
import {DialogModule} from 'primeng/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    GuidesComponent,
    DetailGuidesComponent,
    DeleteGuidesComponent,
    UpdateGuidesComponent,
    AddGuideComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    TreeTableModule,
    ReactiveFormsModule,
    TabViewModule,
    ButtonModule,
    BrowserAnimationsModule,
    DialogModule,
    RouterModule.forChild([
      {path:'guide', children:[
        {path:'', component:GuidesComponent},
        {path:'add', component:AddGuideComponent},
        {path:'update/:id', component:UpdateGuidesComponent},
        {path:'detail/:id', component:DetailGuidesComponent},
      ]}
    ])
  ]
})
export class GuideModule { }
