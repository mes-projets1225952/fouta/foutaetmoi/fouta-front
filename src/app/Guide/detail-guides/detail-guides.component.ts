import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Guide } from 'src/app/Model/guide.model';
import { SiteService } from 'src/app/Service/site.service';


@Component({
  selector: 'app-detail-guides',
  templateUrl: './detail-guides.component.html',
  styleUrls: ['./detail-guides.component.css']
})
export class DetailGuidesComponent implements OnInit{
  public guide: Guide=new Guide();
  selectedFile!: File;
  errorMessage!: string;
  id_guide:any;
  constructor(
    private service:SiteService,
    private route:ActivatedRoute
    ){}
  ngOnInit(): void {
    
    this.id_guide=this.route.snapshot.params['id']
    this.service.geGuideById(this.id_guide).subscribe(data=>{
      this.guide=data
      
      
    })
    
  }
 
}
