import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailGuidesComponent } from './detail-guides.component';

describe('DetailGuidesComponent', () => {
  let component: DetailGuidesComponent;
  let fixture: ComponentFixture<DetailGuidesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DetailGuidesComponent]
    });
    fixture = TestBed.createComponent(DetailGuidesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
