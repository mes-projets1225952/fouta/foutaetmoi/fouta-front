import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Guide } from 'src/app/Model/guide.model';
import { SiteService } from 'src/app/Service/site.service';

@Component({
  selector: 'app-guides',
  templateUrl: './guides.component.html',
  styleUrls: ['./guides.component.css']
})
export class GuidesComponent {
  
  guide: Guide []=[]
  constructor( 
    private siteService:SiteService,
    private http: HttpClient,
    private router:Router

    ){}
  ngOnInit() {
    console.log("site")
    this.siteService.getGuide().subscribe(
  (guide)=>{this.guide= guide
    
    
  },
  (error)=>{
    alert("Problème d'accès à l'API")
    console.log(error)
  }
    )   
}
Test(a:any){
  a="Oumar"

}
deleteGuide(id_guide:number){
  this.siteService.DeleteGuide(id_guide).subscribe(
    (response)=>{
      console.log(response)
      this.ngOnInit(); //Rafraichir la liste
    
    alert("Suppression éffectué!")
    const link=['guide']
    this.router.navigate(link)
  },
  (error)=>{
    alert("Echec")
    console.log(error)
  }
  )
  

}
}

