import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Guide } from 'src/app/Model/guide.model';
import { Site } from 'src/app/Model/site.model';
import { SiteService } from 'src/app/Service/site.service';

@Component({
  selector: 'app-add-guide',
  templateUrl: './add-guide.component.html',
  styleUrls: ['./add-guide.component.css']
})
export class AddGuideComponent {
  public guide: Guide=new Guide();
  public guideForm!: FormGroup;
  selectedFile!: File;
  errorMessage!: string;
  site!: Site [];
  constructor( private siteService: SiteService,
              private router: Router
    ){}
  ngOnInit(): void {
    this.guideForm=new FormGroup({
      nom:new FormControl(),
      age:new FormControl(),
      tel:new FormControl(),
      mail:new FormControl(),
      site_T:new FormControl()
    });
    this.siteService.getSite().subscribe(
      (site)=>{this.site= site
        console.log(this.site)
      },
      (error)=>{
        alert("Problème d'accès à l'API")
        console.log(error)
      }
        )
  }
  
public ajoutGuide() :void{
  var formData:any=new FormData
  formData.append('nom', this.guideForm.get('nom')!.value);
  formData.append('age', this.guideForm.get('age')!.value);
  formData.append('tel', this.guideForm.get('tel')!.value);
  formData.append('mail', this.guideForm.get('mail')!.value);
  formData.append('site_T', this.guideForm.get('site_T')!.value);
  this.siteService.addGuide(formData).subscribe(
    
    (response)=> {
      console.log(response)
      const link=['guide'];
      this.router.navigate(link);
    },
    (error)=>{
      this.errorMessage="Problème de connexion au serveur",
      console.log(error)
    }
  )
  
}

}
