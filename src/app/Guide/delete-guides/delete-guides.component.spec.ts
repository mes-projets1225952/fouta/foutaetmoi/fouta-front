import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteGuidesComponent } from './delete-guides.component';

describe('DeleteGuidesComponent', () => {
  let component: DeleteGuidesComponent;
  let fixture: ComponentFixture<DeleteGuidesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DeleteGuidesComponent]
    });
    fixture = TestBed.createComponent(DeleteGuidesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
