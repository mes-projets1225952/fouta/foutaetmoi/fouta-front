import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Site } from 'src/app/Model/site.model';
import { SiteService } from 'src/app/Service/site.service';

@Component({
  selector: 'app-sites',
  templateUrl: './sites.component.html',
  styleUrls: ['./sites.component.css']
})
export class SitesComponent implements OnInit{
site!: Site [];
    constructor( private siteService:SiteService,
      private http: HttpClient, private router:Router){}
    ngOnInit(): void {
      this.siteService.getSite().subscribe(
        (site)=>{
          this.site= site
        console.log(this.site)},
    
        (error)=>{
          alert("Problème d'accès à l'API")
          console.log(error)
        }
      )
        
  }
  deleteSite(id_site:number){
    this.siteService.DeleteSite(id_site).subscribe(
      (response)=>{
        console.log(response)
        this.ngOnInit(); //Rafraichir la liste
      
      alert("Suppression éffectué!")
      console.log('Bravo', response)
      const link=['site']
      this.router.navigate(link)
    },
    (error)=>{
      alert("Echec")
      console.log(error)
    }
    )
    

  }
}
