import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailSitesComponent } from './detail-sites.component';

describe('DetailSitesComponent', () => {
  let component: DetailSitesComponent;
  let fixture: ComponentFixture<DetailSitesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DetailSitesComponent]
    });
    fixture = TestBed.createComponent(DetailSitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
