import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Site } from 'src/app/Model/site.model';
import { SiteService } from 'src/app/Service/site.service';

@Component({
  selector: 'app-detail-sites',
  templateUrl: './detail-sites.component.html',
  styleUrls: ['./detail-sites.component.css']
})
export class DetailSitesComponent implements OnInit{
  public site: Site=new Site();
  errorMessage!: string;
  id_site: any;
  constructor(
    private service:SiteService,
    private route:ActivatedRoute
    ){}
  ngOnInit(): void {
    this.id_site=this.route.snapshot.params['id']
    this.service.geSiteById(this.id_site).subscribe(data=>{
      this.site=data
      console.log(this.site)
    })
  }
}


