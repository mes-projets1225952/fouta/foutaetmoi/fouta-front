import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SitesComponent } from './sites/sites.component';
import { DetailSitesComponent } from './detail-sites/detail-sites.component';
import { DeleteSitesComponent } from './delete-sites/delete-sites.component';
import { UpdateSitesComponent } from './update-sites/update-sites.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddSiteComponent } from './add-site/add-site.component';
import { ToolbarModule } from 'primeng/toolbar';
import { SplitButtonModule } from 'primeng/splitbutton';



@NgModule({
  declarations: [
    SitesComponent,
    DetailSitesComponent,
    DeleteSitesComponent,
    UpdateSitesComponent,
    AddSiteComponent,
    AddSiteComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(
      [{path:'site', children:[
        {path:'',component:SitesComponent},
        {path:'add', component:AddSiteComponent},
        {path:'modifie/:id', component:UpdateSitesComponent},
        //{path:'update/',component:UpdateSitesComponent},
        {path:'detail/:id',component:DetailSitesComponent},
      ]}]
    ),
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    ToolbarModule,
    SplitButtonModule
  ]
})
export class SiteModule { }
