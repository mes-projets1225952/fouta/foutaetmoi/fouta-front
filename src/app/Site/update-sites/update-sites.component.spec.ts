import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateSitesComponent } from './update-sites.component';

describe('UpdateSitesComponent', () => {
  let component: UpdateSitesComponent;
  let fixture: ComponentFixture<UpdateSitesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UpdateSitesComponent]
    });
    fixture = TestBed.createComponent(UpdateSitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
