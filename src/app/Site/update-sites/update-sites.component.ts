import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Site } from 'src/app/Model/site.model';
import { SiteService } from 'src/app/Service/site.service';

@Component({
  selector: 'app-update-sites',
  templateUrl: './update-sites.component.html',
  styleUrls: ['./update-sites.component.css']
})
export class UpdateSitesComponent  implements OnInit{
  public site: Site=new Site();
  public siteForm!: FormGroup;
  errorMessage!: string;
  data:any
  id_site:any
  constructor(
    private service: SiteService, 
    private route:ActivatedRoute,
    private router: Router){}
  ngOnInit(): void {
    this.id_site=this.route.snapshot.params['id']
    this.service.geSiteById(this.id_site).subscribe(data=>{
      this.siteForm.patchValue(data)
      this.site=data
    })
    this.siteForm=new FormGroup({
      nom:new FormControl(),
      ville:new FormControl(),
      type:new FormControl(),
      photo:new FormControl()
    });
  
}

onFileSelected(event: any) {
  const file =event.target.files![0];
  console.log('file',file)
  console.log('site',this.site)
    this.siteForm.patchValue({
      photo: file,
    });
  this.siteForm.get('photo')!.updateValueAndValidity();
  
  } 
 updateSite(){
  var formData:any=new FormData
  formData.append('nom', this.siteForm.get('nom')?.value);
  formData.append('photo', this.siteForm.get('photo')!.value);
  formData.append('type', this.siteForm.get('type')!.value);
  formData.append('ville', this.siteForm.get('ville')!.value);
  this.service.updateSite(this.id_site,formData).subscribe((data)=>{
    console.log(data)
    this.router.navigate(['site'])
  },
  (error)=>{
    console.log(error)
  })
  
 } 
}
