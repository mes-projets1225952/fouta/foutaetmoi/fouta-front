import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Site } from 'src/app/Model/site.model';
import { SiteService } from 'src/app/Service/site.service';

@Component({
  selector: 'app-add-site',
  templateUrl: './add-site.component.html',
  styleUrls: ['./add-site.component.css']
})
export class AddSiteComponent implements OnInit {
  public site: Site=new Site();
  public siteForm!: FormGroup;
  selectedFile!: File;
  errorMessage!: string;
  
  constructor( private siteService: SiteService,
              private router: Router
    ){}
  ngOnInit(): void {
    this.siteForm=new FormGroup({
      nom:new FormControl(),
      ville:new FormControl(),
      type:new FormControl(),
      photo:new FormControl()
    });
      
  }
onFileSelected(event: any) {
  const file =(event.target as HTMLInputElement).files![0];
  console.log(file)
    this.siteForm.patchValue({
      photo: file,
    
    });
  this.siteForm.get('photo')!.updateValueAndValidity();
  } 
public ajoutSite() :void{
  var formData:any=new FormData
  formData.append('nom', this.siteForm.get('nom')?.value);
  formData.append('photo', this.siteForm.get('photo')!.value);
  formData.append('type', this.siteForm.get('type')!.value);
  formData.append('ville', this.siteForm.get('ville')!.value);
  this.siteService.addSite(formData).subscribe(
    (response)=> {
      console.log(response)
      const link=['site'];
      this.router.navigate(link);
    },
    (error)=>{
      this.errorMessage="Problème de connexion au serveur",
      console.log(error)
    }
  )
  
}
  
}

