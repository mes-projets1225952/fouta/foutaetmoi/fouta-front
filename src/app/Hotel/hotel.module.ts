import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HotelsComponent } from './hotels/hotels.component';
import { ListeHotelComponent } from './liste-hotel/liste-hotel.component';
import { DetailHotelComponent } from './detail-hotel/detail-hotel.component';
import { UpdateHotelComponent } from './update-hotel/update-hotel.component';
import { RouterModule, Routes } from '@angular/router';
import { DeleteGuidesComponent } from '../Guide/delete-guides/delete-guides.component';
import { HomeComponent } from '../home/home.component';



@NgModule({
  declarations: [
    HotelsComponent,
    DetailHotelComponent,
    UpdateHotelComponent,
    ListeHotelComponent
  ],
  imports: [
    RouterModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
   
   
  ]
})
export class HotelModule { }
