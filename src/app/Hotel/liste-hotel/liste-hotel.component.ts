import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Hotel } from 'src/app/Model/hotel.model';
import { SiteService } from 'src/app/Service/site.service';

@Component({
  selector: 'app-liste-hotel',
  templateUrl: './liste-hotel.component.html',
  styleUrls: ['./liste-hotel.component.css']
})
export class ListeHotelComponent implements OnInit{
  hotel!: Hotel [];
  constructor( private siteService:SiteService,
    private http: HttpClient, private router:Router){}
  ngOnInit() {
    console.log("site")
    this.siteService.getSHotel().subscribe(
  (hotel)=>{this.hotel= hotel},
  (error)=>{
    alert("Problème d'accès à l'API")
    console.log(error)
  }
    )
    
}
showHoteleDetails(hotel:Hotel){
this.siteService.showHoteleDetails(hotel)
console.log(hotel)
}
deleteHotel(id_hotel:number){
  this.siteService.DeleteHotel(id_hotel).subscribe(
    (response)=>{
      console.log(response)
      this.ngOnInit(); //Rafraichir la liste
    
    alert("Suppression éffectué!")
    const link=['hotel']
    this.router.navigate(link)
  },
  (error)=>{
    alert("Echec")
    console.log(error)
  }
  )
  

}
}
