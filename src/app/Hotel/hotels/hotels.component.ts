import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Hotel } from 'src/app/Model/hotel.model';
import { SiteService } from 'src/app/Service/site.service';

@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.css']
})
export class HotelsComponent {

  public hotel: Hotel=new Hotel();
  public hotelForm!: FormGroup;
  errorMessage!: string;
  photo:any
  constructor( private hotelService: SiteService,
              private router: Router
    ){}
  ngOnInit(): void {
    this.hotelForm=new FormGroup({
      nom:new FormControl(),
      type:new FormControl(),
      ville:new FormControl(),
      photo:new FormControl()
    });
      
  }
  onFileSelected(event: any) {
    const file =(event.target as HTMLInputElement).files![0];
    console.log(file)
    this.photo=file
      this.hotelForm.patchValue({
        photo: file,
        
      
      });
    this.hotelForm.get('photo')!.updateValueAndValidity();
    } 
  public ajouthotel() :void{
    var formData:any=new FormData
    formData.append('nom', this.hotelForm.get('nom')?.value);
    formData.append('photo',this.photo, this.photo?.name);
    formData.append('type', this.hotelForm.get('type')!.value);
    formData.append('ville', this.hotelForm.get('ville')!.value);
    this.hotelService.addHotel(formData).subscribe(
      (response)=> {
        console.log(response)
        const link=['hotel'];
        this.router.navigate(link);
      },
      (error)=>{
        this.errorMessage="Problème de connexion au serveur",
        console.log(error)
      }
    )
    
  }
    
  }
  
  