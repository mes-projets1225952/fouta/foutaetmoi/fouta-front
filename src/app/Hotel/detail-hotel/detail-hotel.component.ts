import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Hotel } from 'src/app/Model/hotel.model';
import { SiteService } from 'src/app/Service/site.service';

@Component({
  selector: 'app-detail-hotel',
  templateUrl: './detail-hotel.component.html',
  styleUrls: ['./detail-hotel.component.css']
})
export class DetailHotelComponent implements OnInit{
 
  public hotel: Hotel=new Hotel();
  errorMessage!: string;
  id_hotel: any;
  constructor(
    private service:SiteService,
    private route:ActivatedRoute
    ){}
  ngOnInit(): void {
    this.id_hotel=this.route.snapshot.params['id']
    this.service.geHotelById(this.id_hotel).subscribe(data=>{
      this.hotel=data
      console.log(this.hotel)
    })
  }
}
