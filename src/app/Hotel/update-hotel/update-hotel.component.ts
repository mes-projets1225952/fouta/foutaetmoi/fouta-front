import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Hotel } from 'src/app/Model/hotel.model';
import { SiteService } from 'src/app/Service/site.service';

@Component({
  selector: 'app-update-hotel',
  templateUrl: './update-hotel.component.html',
  styleUrls: ['./update-hotel.component.css']
})
export class UpdateHotelComponent implements OnInit{

  constructor(private service: SiteService, 
    private route:ActivatedRoute,
    private router: Router){}
    public hotel: Hotel=new Hotel();
    public hotelForm!: FormGroup;
    selectedFile!: File;
    errorMessage!: string;
    data:any
    id_hotel:any
    ngOnInit(): void {
        
  this.id_hotel=this.route.snapshot.params['id']
    this.service.geHotelById(this.id_hotel).subscribe(data=>{
      this.hotelForm.patchValue(data)
      this.hotel=data
      console.log(this.hotel)
    })
    this.hotelForm=new FormGroup({
      nom:new FormControl(),
      ville:new FormControl(),
      type:new FormControl(),
      photo:new FormControl()
    });
  
}

onFileSelected(event: any) {
  console.log('bien')
  const file =event.target.files![0];
  console.log(file)
    this.hotelForm.patchValue({
      photo: file,
    });
  this.hotelForm.get('photo')!.updateValueAndValidity();
  } 
 updateHotel(){
  var formData:any=new FormData
  formData.append('nom', this.hotelForm.get('nom')?.value);
  formData.append('photo', this.hotelForm.get('photo')!.value);
  formData.append('type', this.hotelForm.get('type')!.value);
  formData.append('ville', this.hotelForm.get('ville')!.value);
  this.service.updateHotel(this.id_hotel,formData).subscribe((data)=>{
    console.log(data)
    this.router.navigate(['hotel'])
  },
  (error)=>{
    console.log(error)
  })
  
 } 
}


