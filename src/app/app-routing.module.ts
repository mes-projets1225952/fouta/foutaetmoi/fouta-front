import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListeHotelComponent } from './Hotel/liste-hotel/liste-hotel.component';
import { HotelsComponent } from './Hotel/hotels/hotels.component';
import { DetailHotelComponent } from './Hotel/detail-hotel/detail-hotel.component';
import { UpdateHotelComponent } from './Hotel/update-hotel/update-hotel.component';
import { DeleteGuidesComponent } from './Guide/delete-guides/delete-guides.component';
import { HomeComponent } from './home/home.component';

import { HotelModule } from './Hotel/hotel.module';

/*const routes: Routes = [
        
        {path:'',component:HomeComponent},
        {
          path: 'hotel',
          loadChildren: () => import('./Hotel/hotel.module').then(m => m.HotelModule)
        }
      
];*/
const routes: Routes = [
  {path:'hotel', children:[
    {path:'', component:ListeHotelComponent},
    {path:'add', component:HotelsComponent},
    {path:'detail/:id', component:DetailHotelComponent},
    {path:'update/:id', component:UpdateHotelComponent},
  
  ]},
  
  {path:'',component:HomeComponent},
  
 

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
